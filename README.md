# Challenge_2_MDEF_Sheet_Mask

Week from March 15 to 19 2021. Challenge n ° 2 of the MDEF Master. I worked with Bothaina around cosmetics. We chose to continue working on sheet masks for this second challenge.

<html>

<!-- !PAGE CONTENT! -->
<div class="w3-content" style="max-width:1500px">

  <!-- Header -->
  <div class="w3-row">
  <div class="w3-half w3-container">
    <h1 class="w3-xxlarge w3-text-light-grey">March 2021</h1>
    <h1 class="w3-xxlarge w3-text-grey">Week 15/19 </h1>
    <h1 class="w3-jumbo">Challenge 2</h1>
    <h1 class="w3-xxlarge w3-text-light-grey">Experimenting with 3D printing and CNC.</h1>
    <h1 class="w3-xxlarge w3-text-grey"> </h1>
    <h1 class="w3-jumbo">Sheet masks</h1>
  </div>
  <div class="w3-half w3-container w3-xxlarge w3-text-grey">
      <p class=""> CNC (Computer Numerical control) </p>
  </div>
  </div>

  <!-- Photo Grid -->

![](Images/Rhino_prototype_2.JPG)

<p class="w3-xlarge">
We wanted to make 3 main changes
</p>

<p class="w3-xlarge">
1.The thickness of the sheet mask.
</p>

<p class="w3-xlarge">
2.The edges of the sheet mask.
</p>

<p class="w3-xlarge">
3.The shapes of the parts.
</p>

<p class="w3-xlarge">
In this challenge, we wanted to change the prototype mold we created for our first challenge. By changing the thickness of the sheet mask, the edges and the shapes of the parts.

For the thickness of the sheet mask we decided for the second prototype to use the CNC and engrave our mold by using the 10mm acrylic material (thick enough not to break and dig 2.5mm thick) 2.5mm is the thickness of the masks we want to have for our face. We have changed the Rhino file of the first challenge (which you can find by going to the repository of challenge 1) and modify it to engrave it ,with the same dimensions and curved edges.

We started by :
Creating the shape in 3D inventor and exporting it as STL as 2D shape.
We made 2 files to cut on the CNC with KinetiC-NC software
One for the flat engraving that we needed for the mold [One for the flat engraving that we needed for the mold](Files/6mmflat.nc)
[One for the curved edges](Files/face_6mm round.nc)
We used the 10mm transparent acrylic material and fixed it to make it more stable for the CNC machine with screws directly on the board.

</p>

![](Images/process_CNC.JPG)

![](Images/process_cnc_1.JPG)

<p class="w3-xlarge">

This is the final result ! The mold here is filled up with a face mask recipe.

Recipe :

<p class="w3-xlarge">
5 ml Agar-agar
</p>

<p class="w3-xlarge">
150 ml Tea</p>

<p class="w3-xlarge">
1 gr Orange peels</p>

<p class="w3-xlarge">
1 gr Coffee grounds</p>

</p>

![](Images/mold_prototype_2ok.JPG)

<p class="w3-xlarge">
See the results on the following images. The first image shows a thicker mask with rectangular edges from the first prototype mold. The second photo shows a thinner mask with rounded edges obtained with the second prototype mold.
</p>

![](Images/first_picture_thick.JPG)

![](Images/secondpicture_thin.JPG)

<p class="w3-xlarge">
With the use of CNC we have noticed that the process is easier than we think, we understand that we need to have thicker acrylic to make sure it will not break when we engrave it.
</p>

<h1 class="w3-jumbo"> 3D printing </h1>

<p class="w3-xlarge">

For the shapes of the parts, we decided to create 3D faces to try out different shapes for the masks using a 3D printer.
We started with:
Scan the face using ScandyPro (we could not save the scan because the program was being repaired).
</p>

![](Images/Kris_1.JPG)

<p class="w3-xlarge">
Scan the face using the Capture app (the scanned images were not what we wanted). We did not get enough data on the face to be able to use it.
</p>

![](Images/Anais_capture.JPG)

<p class="w3-xlarge">
The face scan using Kinet xbox 360 is the one we used to 3D print the final face shapes. We did a full scan of the face, circling the person for more details. We then downloaded the file after correcting it with Skanet and exporting to STL.
</p>

![](Images/Bothaina_kinet.JPG)

<p class="w3-xlarge">
Once we download the STL files from Skanet we cleaned them and filled the face to prepare it for the 3d printer by using Rhino / Blender / Meshmixer softwares.
We had two 3D printing faces :
</p>

<p class="w3-xlarge">
A man face to make masks personalized, to see how facial hair will effect the shapes (we cutout the cheeks).
</p>

[Man 3D printing file.](Files/krisCARAclosedfinal2.stl)

![](Images/Kris_3Dprinting.JPG)

<p class="w3-xlarge">
A woman face.
</p>

[Woman 3D printing file.](Files/bothaniaFace2.stl)

![](Images/Bothaina_3Dprinting.JPG)

<p class="w3-xlarge">
Using the 3D printer, we noticed that the process was more difficult than thought, cleaning up the files was complicated because the shapes are organic shapes.

</p>

<h1 class="w3-jumbo">Reflection</h1>

<p class="w3-xlarge">

We did some testing with the new mold and tried the same recipe as above.
Here you can see the results.

</p>

![](Images/shapes_3Dprintingok.JPG)

<p class="w3-xlarge">

We wanted to use 3D printing of the faces to further customize the character of the masks. We had a lot of trouble cleaning the file in SLT scans. We have used many tutorial links (1)(2)(3)(4) to clean up the scan so that we can use it for the 3D printer. We also had the question of the scale. We didn't print it at scale 1, it's a bit smaller that will affect our results with the shape masks. We wanted to use digital manufacturing to make cosmetics and use technology to our advantage. We still have to continue the project but we have learned a lot about our possibilities.

The next steps will be to use the scan and 3D printing to make personalized mold by using the data of the person face. We also want to explore other material than acrylic, like ceramic.

Another angle of this project and in union with the knowledge we want to acquire about our body, we want to explore the microbiome of our skin. We did some research and found some interesting references like "Future Flora" of Guilia Tomasello (5) with whom we had an appointment last week which opened up another line of reading for our project. We want to take a sample and then proceed to a culture of bacteria from our different parts of the face using our mold.(6) The next steps would be to prototype the kind of material we can use to ensure sterility and these cultures.(7) (8)

</p>

<p class="w3-xlarge">
Bibliography :
</p>

<p class="w3-large ">
<a href="http://blenderclan.tuxfamily.org/html/modules/content/index.php?id=91">(1) Organic tutorial modeling.</a></p>

<p class="w3-large ">
<a href="https://blender.stackexchange.com/questions/92918/how-do-you-cut-off-part-of-a-mesh-you-dont-want">(2) How do you cut off part of a mesh you don't want?.</a></p>

<p class="w3-large ">
<a href="https://www.youtube.com/watch?v=oc9ninRRXGo">(3) How to union, difference, intersect objects | Boolean modifier | Blender 2.81 | Tutorial.</a></p>

<p class="w3-large ">
<a href="https://www.youtube.com/watch?v=0I-4a1q0YLE">(4) Knife Tool - Blender 2.80 Fundamentals.</a></p>

<p class="w3-large ">
<a href="https://gitomasello.com">(5) "Future Flora" of Guilia Tomasello.</a></p>

<p class="w3-large ">
<a href="https://www.labiotech.eu/more-news/mellissa-fisher-microbial-portrait/">(6) A Self-Portrait of the Microbes that Live on our Skin, by Clara Rodriguez Fernandez around Mellissa Fisher.</a></p>

<p class="w3-large ">
<a href="https://biofriction.org/biofriction/maddalena-fragnito-and-zoe-romano-kinlab/">(7) Biofriction, Barcelona.</a></p>

<p class="w3-large ">
<a href="https://biofriction.org/biofriction/biolab-conference/">(8) Biofriction conferences.</a></p>

<!-- End Page Content -->
</div>


</body>
</html>
